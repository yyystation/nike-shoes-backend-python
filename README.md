### Nike Interview Backend Code Challenge [Python]

_The provided code document should contain more details._

Start server (Port 8081): `python api.py` (you need to install `python`, `pip` & `flask` on your machine)

APIs:

1. Clone [this project](https://gitlab.com/hiring_nike_china/fetch-shoe-prices/) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```
